#include "Controller.h"
#include "common.h"
#include "Window.h"
#include "Game.h"

#include <cstdio>
#include <cstdlib>

Controller::Controller(Game *game, bool isAuto)
: _game(game), _is_auto(isAuto)
{
  rep(i, 8)
    rep(j, 8) {
      _is_empty[i][j] = true;
      _is_highlighted[i][j] = false;
			_highlight[i][j] = false;
      _piece[i][j] = 0;
    }
  _has_highlight = false;
	_is_playing = false;
  
  _game->fill_pieces(&**_is_empty, &**_piece);
}

Controller::~Controller()
{
}

void Controller::click(int x, int y)
{
  printf("Click: (%i, %i)\n", x, y);

	if(!_is_playing) return;
  
  if(_has_highlight) {
    if(_is_highlighted[x][y]) {
      if(x == _last_click_x && y == _last_click_y) {
        clear_highlight();
      } else {
        printf("action()\n");
        
        // realiza movimento de (_last_click_x, _last_click_y) para (x, y)
        _game->move(_last_click_x, _last_click_y, x, y);
				update_all();
      }
    } else {
      clear_highlight();
    }
  } else {
    clear_highlight();
    if(can_highlight(x, y)) {
      _has_highlight = true;
      _is_highlighted[x][y] = true;
      _highlight[x][y] = 1;
      
      // possibilidades de movimento
      _game->fill_moves(x, y, &**_is_highlighted);
      
      _last_click_x = x;
      _last_click_y = y;
    }
  }
}

void Controller::set_minimax_depth(int minimaxDepth)
{
  printf("minimaxDepth: %i\n", minimaxDepth);
	_minimax_depth = minimaxDepth;
	_game->set_minimax_depth(minimaxDepth);
}

void Controller::set_first_move(int firstMove)
{
  printf("firstMove: %i\n", firstMove);
  _game->set_player(firstMove);
}

void Controller::set_computer_color(int computerColor)
{
  printf("computerColor: %i\n", computerColor);
	_game->set_computer_color(computerColor);
}

void Controller::set_window(Window *window)
{
  _window = window;
}

#include "MinimaxEnemy.h"

#define NUM_POP 10
#define NUM_LIVE (NUM_POP / 2)
#define MAX_REPS 100
#define PROP_MUT 10
#define MAX_MOVES 100

typedef struct {
	MinimaxEnemy::HeuType heu[NUM_HEU];
	MinimaxEnemy *e;
	int wins;
} Being;

int compBeing(const void *l, const void *r)
{
	return ((Being *)r)->wins - ((Being *)l)->wins;
}
  
void Controller::play()
{
	//int max_n_moves = -1;
	if(_is_auto) {
		srand((unsigned) time(NULL));
		Being pop[NUM_POP];
		rep(j, NUM_POP) {
			rep(k, NUM_HEU) {
				pop[j].heu[k] = rand() / (double) RAND_MAX;
			}
			pop[j].e = new MinimaxEnemy(0, _minimax_depth);
			pop[j].e->set_weights(pop[j].heu);
		}
		rep(r, MAX_REPS) {
			rep(i, NUM_POP) {
				pop[i].wins = 0;
			}
			
			printf("---------------------------\n");
			printf("Ger: %5i\n", r);
			printf("---------------------------\n");
			rep(i, NUM_POP) {
				printf("%2i ", pop[i].wins);
				rep(j, NUM_HEU) {
					printf("%.5f ", pop[i].heu[j]);
				}
				printf("\n");
			}
			printf("---------------------------\n\n");

			rep(i, NUM_POP) {
				pop[i].e->set_player(0);
				rep(j, NUM_POP) {
					if(i == j) continue;
					printf("%02i x %02i - ", i, j);
					pop[j].e->set_player(1);
					_game->set_players(pop[i].e, pop[j].e);
					_game->reset();
					//_game->print();
					int n_moves = 0;
					while(_game->think() && n_moves++ < MAX_MOVES) {
						//_game->print();
						//if(n_moves > max_n_moves) {
							//max_n_moves = n_moves;
							//printf("\n\n======================\nmax_n_moves: %i\n========================\n\n", max_n_moves);
						//}
						if(_game->is_end()) {
							//_game->print();
							break;
						}
					}
					if(_game->get_winner()) {
						pop[j].wins++;
					} else {
						pop[i].wins++;
					}
				}
			}
			qsort(pop, NUM_POP, sizeof(Being), compBeing);
			repb(i, NUM_LIVE, NUM_POP) {
				int j = rand() % NUM_LIVE,
						k = rand() % NUM_LIVE;
				while(k == j) k = rand() % NUM_LIVE;
				rep(l, NUM_HEU) {
					if(rand() % 2) {
						pop[i].heu[l] = pop[j].heu[l];
					} else {
						pop[i].heu[l] = pop[k].heu[l];
					}
					if(rand() % PROP_MUT == 0) {
						pop[i].heu[l] = rand() / (double) RAND_MAX;
					}
				}
				pop[i].e->set_weights(pop[i].heu);
			}
			printf("\n\n\n+++++++++++++++++++++++++++\n");
			printf("%i %i %i %i", NUM_POP, NUM_LIVE, PROP_MUT, MAX_REPS);
			printf("\n+++++++++++++++++++++++++++\n%2i ", pop[0].wins);
			rep(i, NUM_HEU) {
				printf("%.5f ", pop[0].heu[i]);
			}
			printf("\n+++++++++++++++++++++++++++\n\n");
			//printf("endgame winner: %i\n", _game->get_winner());
		}
	} else {
		printf("play()\n");
		_is_playing = true;
		_window->disable_all();
		_window->enable("stop");
		update_all();
	}
	//printf("\n\n======================\nmax_n_moves: %i\n========================\n\n", max_n_moves);
}

void Controller::stop()
{
  //TODO
  printf("stop()\n");
	_is_playing = false;
  _window->enable_all();
  _window->disable("stop");
	update_all();
}

void Controller::undo()
{
	_game->undo();
	update_all();
}

void Controller::redo()
{
	_game->redo();
	update_all();
}

void Controller::load(FILE *in)
{
	_game->load(in);
	update_all();
}

void Controller::after_display()
{
	if(_is_playing && _game->think()) {
		update_all();
	}
}

bool Controller::is_empty(int x, int y)
{
  return _is_empty[x][y];
}

int Controller::piece(int x, int y)
{
  return _piece[x][y];
}

bool Controller::is_highlighted(int x, int y)
{
  return _is_highlighted[x][y];
}

int Controller::highlight(int x, int y)
{
  return _highlight[x][y];
}

void Controller::clear_highlight()
{
  rep(i, 8)
    rep(j, 8) {
      _is_highlighted[i][j] = false;
			_highlight[i][j] = false;
		}
  _has_highlight = false;
  _last_click_x = _last_click_y = -1;
}

bool Controller::can_highlight(int x, int y)
{
  return _game->can_move(x, y);
}

void Controller::update_all()
{
	int fromX, fromY, toX, toY;
  _game->fill_pieces(&**_is_empty, &**_piece);
	clear_highlight();
	_game->get_last_move(fromX, fromY, toX, toY);
	if(fromX != -1) {
		_is_highlighted[fromX][fromY] = _is_highlighted[toX][toY] = true;
		_highlight[fromX][fromY] = _highlight[toX][toY] = 1;
	}
	if(_game->can_undo()) _window->enable("undo");
	else _window->disable("undo");
	if(_game->can_redo()) _window->enable("redo");
	else _window->disable("redo");
	if(_game->is_end() && _is_playing) {
		stop();
		//fflush(stdout);
	}
	_window->display();
	//printf("digite\n");
	//fflush(stdout);
	//scanf("%*c");
}
