#ifndef __IA_GAME_H__
#define __IA_GAME_H__

#include <cstdio>

class Board;
class MoveHistoryEntry;
class Enemy;

class Game
{
public:
  Game();
  ~Game();
  void move(int fromX, int fromY, int toX, int toY);
  bool think();
  void fill_moves(int x, int y, bool *to);
  bool can_move(int x, int y);
  void fill_pieces(bool *isEmpty, char *piece);
  void set_player(bool player);
	void set_players(Enemy *a, Enemy *b);
	bool can_undo() { return _current_move > 1; }
	bool can_redo() { return _current_move <= _move_count; }
	void undo();
	void redo();
	void reset();
	void get_last_move(int& fromX, int &fromY, int& toX, int& toY);
	void load(FILE *in);
	void set(MoveHistoryEntry entry);
	void set_computer_color(int computerColor) { _computer_color = computerColor; }
	void set_minimax_depth(int minimaxDepth);
	bool is_end();
	bool get_winner() { return !_player; }
	void print();
  
private:
  Board *_board;
  bool _player;
	int _move_count, _current_move, _computer_color, _minimax_depth;
	MoveHistoryEntry *_move_history;
	FILE *_out;
	Enemy *_enemy[2];
  
};

#endif
