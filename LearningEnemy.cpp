#include "LearningEnemy.h"
#include "Board.h"

#define SQU(x) ((x) * (x))
#define MOD(x) (x) > 0 ? (x) : -(x)
#define MAX_ALPHABETA 1000000


int LearningEnemy::heuristica(Board board, bool player)
{
    return (int)(d[0]*distCentro(board,player) + d[1]*distEntreSiMan(board,player) + 
           d[2]*distEntreSiEuc(board,player) + d[3]*cubicPolynome(board,player) -
           d[4]*(d[0]*distCentro(board,!player) + d[1]*distEntreSiMan(board,!player) + 
           d[2]*distEntreSiEuc(board,!player) + d[3]*cubicPolynome(board,!player)));
}

// heurística: minimizar a soma das distâncias das nossas peças para o centro do tabuleiro
double LearningEnemy::distCentro(Board board, bool player)
{
	double res = 0;
	rep(i, 8)
		rep(j, 8)
			if(board.get(player, i, j))
				res += SQU(i - 3.5) + SQU(j - 3.5); // distância para o centro
	return res;
}

// heurística: minimizar a soma das distâncias de manhattan entre nossas peças
double LearningEnemy::distEntreSiMan(Board board, bool player)
{
	double res = 0;
	rep(i, 8)
		rep(j, 8)
			if(board.get(player, i, j))
                rep(k, 8)
                    rep(l, 8)
                        if(board.get(player, k, l))
				            res += MOD(i - k) + MOD(j - l); // distancia entre pontos
	return res;
}

// heurística: minimizar a soma das distâncias de euclides entre nossas peças
double LearningEnemy::distEntreSiEuc(Board board, bool player)
{
	double res = 0;
	rep(i, 8)
		rep(j, 8)
			if(board.get(player, i, j))
                rep(k, 8)
                    rep(l, 8)
                        if(board.get(player, k, l))
				            res += SQU(i - k) + SQU(j - l); // distancia entre pontos
	return res;
}

// heurística: minimizar polinomio cubico sobre as posicoes das nossas pecas
double LearningEnemy::cubicPolynome(Board board, bool player)
{
    /* Precisa ser otimizado */
    double res = 0;
	rep(i, 8)
		rep(j, 8)
			if(board.get(player, i, j))
			    res += d[5+INDEX(i,j)]*i*i*i + d[5+63+INDEX(i,j)]*i*i +
                d[5+2*63+INDEX(i,j)]*i + d[5+3*63+INDEX(i,j)]*j*j*j +
                d[5+4*63+INDEX(i,j)]*j*j + d[5+5*63+INDEX(i,j)]*j;
    return 0;
}

LearningEnemy * LearningEnemy::createSon()
{
    double bump;
    double diff;
    LearningEnemy * novo = new LearningEnemy(_player,_minimax_depth);
    /* Para cada variavel, criar uma variavel semelhante a do pai com um bump aleatorio
    que eh inversamente proporcional ao numero de jogos e com uma variacao aleatoria que
    depende das ultimas variacoes e do numero de vitorias e derrotas da ultima rodada e
    do numero de vitorias e derrotas geral. */
    rep(i, VAR_DOU) {
        bump = /* valor entre -1 e 1, dividido pelo n�mero de jogos totais */ 0;
        diff = 3*d_int[0][i] + 2*d_int[1][i] + d_int[2][i];
        diff = /* valor entre -diff e diff, mais positivo quanto mais vitorias na ultima rodada */
        novo->d[i] = d[i] + bump + diff;
        /* atribuir nova derivada */
    }
    return novo;
}

LearningEnemy * LearningEnemy::createSon(LearningEnemy enemy)
{
    double bump;
    double diff;
    LearningEnemy * novo = new LearningEnemy(_player,_minimax_depth);
    /* Mesmo que o outro, por�m escolhendo como base a vari�vel de um dos pais, proporcionalmente
    ao percentual de vitorias de cada um */
    return novo;
}
