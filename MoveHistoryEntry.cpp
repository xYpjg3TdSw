#include "MoveHistoryEntry.h"
#include "Board.h"

#include <cstdio>

MoveHistoryEntry::MoveHistoryEntry() 
{
	_board = NULL;
	_from_x = _from_y = _to_x = _to_y = -1;
}

MoveHistoryEntry::MoveHistoryEntry(Board *board, int fromX, int fromY, int toX, int toY)
{
	_board = board->copy();
	_from_x = fromX;
	_from_y = fromY;
	_to_x = toX;
	_to_y = toY;
}

const MoveHistoryEntry& MoveHistoryEntry::operator=(const MoveHistoryEntry& entry)
{
	if(_board) delete _board;
	_board = entry._board;
	_from_x = entry._from_x;
	_from_y = entry._from_y;
	_to_x = entry._to_x;
	_to_y = entry._to_y;
	return *this;
}
