#ifndef __MINIMAX_ENEMY_H__
#define __MINIMAX_ENEMY_H__

#include "Enemy.h"
#include "Board.h"

#include <ctime>
#include <cstdlib>
//#include <iostream>
//using namespace std;

#include <cfloat>
#define HeuTypeMax DBL_MAX
#define NUM_HEU 8

class MinimaxEnemy: public Enemy
{
public:
	typedef double HeuType;
	MinimaxEnemy(bool player, int minimaxDepth): Enemy(player, minimaxDepth)/*, _game_boards(board_compare)*/ {
		rep(i, NUM_HEU)
			_heu[i] = 1.;
		_heu_total = NUM_HEU;
	}
	void move(Board *board, int& fromX, int& fromY, int& toX, int& toY);
	HeuType alphabeta(const Board &board, int depth, HeuType alpha, HeuType beta, int player);
	virtual 
	HeuType heuristica(Board &board, bool player, bool plastmove);
	HeuType centro(Board &board, bool player, bool plastmove);
	HeuType concentration(Board &board, bool player, bool plastmove);
	HeuType centralisation(Board &board, bool player, bool plastmove);
	HeuType masscenter(Board &board, bool player, bool plastmove);
	HeuType quads(Board &board, bool player, bool plastmove);
	HeuType connectedness(Board &board, bool player, bool plastmove);
	HeuType uniformity(Board &board, bool player, bool plastmove);
	void set_weights(HeuType heu[NUM_HEU]);
	virtual void undo(Board *board) { 
		//_game_boards.erase(board->to_comp()); 
		/*
		for(Board::Set::iterator i = _game_boards.begin(); i != _game_boards.end(); ++i) {
			cout << i->first << ' ' << i->second << endl;
		}
		cout << endl;//*/
	}
protected:
	int _choice;
	HeuType _heu[NUM_HEU], _heu_total;
	//Board::Set _game_boards;
};

#endif
