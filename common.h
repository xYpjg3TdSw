#ifndef __IA_MACROS_H__
#define __IA_MACROS_H__

#define MIN(x, y) ((x) < (y) ? (x) : (y))

#define rep(i, n) repi(i, n, 1)
#define repi(i, n, x) for(int i = 0; i < n; i += x)
#define repbe(i, b, n) repbc(i, b, n, <=)
#define repb(i, b, n) repbc(i, b, n, <)
#define repbc(i, b, n, c) for(int i = b; i c n; ++i)
#define reps(i, n) for(unsigned int i = 0; i < n.size(); ++i)

#define INDEX(i, j) (((i) << 3) + (j))

#endif
