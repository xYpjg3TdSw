#ifndef __IA_CONTROLLER_H__
#define __IA_CONTROLLER_H__

#include <cstdio>

class Window;
class Game;

class Controller
{
public:
  Controller(Game *game, bool isAuto = false);
  ~Controller();
  
  void click(int x, int y);
  
  void set_minimax_depth(int minimaxDepth);
  void set_first_move(int firstMove);
  void set_computer_color(int computerColor);
  void set_window(Window *window);
  
  void play();
  void stop();
  void undo();
  void redo();
	void load(FILE *in);
	/* called after render scene
	 * checks if its the computer turn to play
	 */
	void after_display();
  
  bool is_empty(int x, int y);
  int piece(int x, int y);
  bool is_highlighted(int x, int y);
  int highlight(int x, int y);
  
private:
  void clear_highlight();
  bool can_highlight(int x, int y);
	void update_all();
  
  Window *_window;
  bool _is_highlighted[8][8], _is_empty[8][8], _has_highlight, _is_playing;
  char _highlight[8][8], _piece[8][8];
  int _last_click_x, _last_click_y;
  Game *_game;
	bool _is_auto;
	int _minimax_depth;
};

#endif
