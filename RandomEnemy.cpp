#include "RandomEnemy.h"
#include "Board.h"

#include <cstdio>

void RandomEnemy::move(Board *board, int& fromX, int& fromY, int& toX, int& toY)
{
	_board = board;

  Board::MoveListOt moveList;
	int choice = rand() % _board->all_moves(_player, moveList);

	/*
	printf("choice: %i\n", choice);
  reps(i, moveList) {
		printf("%i: (%i, %i) -> (%i, %i)\n", i, INDEX_X(moveList[i].first), INDEX_Y(moveList[i].first)
				                           , INDEX_X(moveList[i].second), INDEX_Y(moveList[i].second));
		fflush(stdout);
  } //*/

	fromX = INDEX_X(INDEX_FROM(moveList[choice]));
	fromY = INDEX_Y(INDEX_FROM(moveList[choice]));
	toX = INDEX_X(INDEX_TO(moveList[choice]));
	toY = INDEX_Y(INDEX_TO(moveList[choice]));
}
