#ifndef __LEARNING_ENEMY_H__
#define __LEARNING_ENEMY_H__

#include "MinimaxEnemy.h"

#define VAR_INT 100
#define VAR_DOU 100
#define STEPS   3

class LearningEnemy: public MinimaxEnemy
{
public:
    LearningEnemy(bool player, int minimaxDepth): MinimaxEnemy(player, minimaxDepth) {}
    ~LearningEnemy();
    LearningEnemy * createSon();
    LearningEnemy * createSon(LearningEnemy enemy);
    unsigned int vitorias;
    unsigned int derrotas;
    unsigned int rodadaVitorias;
    unsigned int rodadaDerrotas;
    int i[VAR_INT];
    int d_int[STEPS][VAR_INT]; //    Derivada nos �ltimos passos
    double d[VAR_DOU];
    double d_dou[STEPS][VAR_DOU]; // Derivada nos �ltimos passos
	int heuristica(Board board, bool player);
private:
    double distCentro(Board board, bool player);
    double distEntreSiMan(Board board, bool player);
    double distEntreSiEuc(Board board, bool player);
    double cubicPolynome(Board board, bool player);
};

#endif
