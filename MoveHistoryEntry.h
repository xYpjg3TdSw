#ifndef __MOVE_HISTORY_ENTRY_H__
#define __MOVE_HISTORY_ENTRY_H__

class Board;

class MoveHistoryEntry
{
public:
	MoveHistoryEntry(); 
	MoveHistoryEntry(Board *board, int fromX, int fromY, int toX, int toY);
	const MoveHistoryEntry& operator=(const MoveHistoryEntry& entry);

	Board *board() { return _board; }
	int from_x() { return _from_x; }
	int from_y() { return _from_y; }
	int to_x()   { return _to_x; }
	int to_y()   { return _to_y; }

private:
	Board *_board;
	int _from_x, _from_y, _to_x, _to_y;

};

#endif
