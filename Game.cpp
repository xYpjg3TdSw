#include "Game.h"
#include "Board.h"
#include "MoveHistoryEntry.h"
#include "RandomEnemy.h"
#include "MinimaxEnemy.h"

#include <cstdio>
#include <ctime>
#include <cassert>

/* nome do arquivo de log */
#define LOG_FILE "log.txt"
/* número máximo de movimentos de uma partida */
#define MAX_MOVES 1000

Game::Game()
{
	assert(sizeof(Board::ImpComp) == 8);
  _board = new Board();
  _board->initial_position();
  _player = 0;
	_move_count = _current_move = 0;
	_computer_color = 0;
	_move_history = new MoveHistoryEntry[MAX_MOVES];
	_move_history[_move_count = _current_move++] = MoveHistoryEntry(_board, -1, -1, -1, -1);
	_out = fopen(LOG_FILE, "a");
	assert(_out);
	MinimaxEnemy *t = new MinimaxEnemy(0, _minimax_depth);
	t->set_weights(((HeuType [NUM_HEU]) {0., 0.82723, 0., 0.97915, 0., 0.96032, 0.96950, 0.}));
	_enemy[0] = t;
	t = new MinimaxEnemy(1, _minimax_depth);
	t->set_weights(((HeuType [NUM_HEU]) {0., 0.82723, 0., 0.97915, 0., 0.96032, 0.96950, 0.}));
	_enemy[1] = t;
	//_enemy[1] = new MinimaxEnemy(1, _minimax_depth);
	//_enemy[1] = new RandomEnemy(1, _minimax_depth);

	time_t  currtime;
	char charTime[100] = {0};
	time(&currtime);
	strftime(charTime,sizeof(charTime)-1,"%c",localtime(&currtime));
	fprintf(_out, "time: %s\n", charTime);
}

Game::~Game()
{
  delete _board;
	delete _move_history;
	fclose(_out);
}

void Game::move(int fromX, int fromY, int toX, int toY)
{
  _board->move(_player, Board::Move(INDEX(fromX, fromY), INDEX(toX, toY)));
  _player = !_player;
	//if(can_redo())
		//fprintf(_out, "back: %i\n", _move_count - _current_move + 1);
	_move_history[_move_count = _current_move++] = MoveHistoryEntry(_board, fromX, fromY, toX, toY);
	//fprintf(_out, "%i: %i %i %i %i\n", _move_count, fromX, fromY, toX, toY);
}

bool Game::think()
{
  //TODO
	if(_computer_color & (1 << _player)) {
		//printf("Computer Turn\n");
		//fflush(stdout);
		int fromX, fromY, toX, toY;
		_enemy[_player]->move(_board, fromX, fromY, toX, toY);
		move(fromX, fromY, toX, toY);
		return true;
	} else {
		//printf("Player Turn\n");
		return false;
	}
}

void Game::fill_moves(int x, int y, bool *to)
{
  Board::MoveList moveList;
  _board->moves(_player, x, y, moveList);
  
  reps(i, moveList) {
    to[(int) moveList[i].second] = 1;
  }
}

bool Game::can_move(int x, int y)
{
  return _board->get(_player, x, y);
}

void Game::fill_pieces(bool *isEmpty, char *piece)
{
  rep(i, 8)
    rep(j, 8)
      isEmpty[INDEX(i, j)] = !(_board->get(0, i, j) || _board->get(1, i, j));
  
  rep(i, 8)
    rep(j, 8)
      if(_board->get(1, i, j))
        piece[INDEX(i, j)] = 1;
			else piece[INDEX(i, j)] = 0;
}

void Game::set_player(bool player)
{
  _player = player;
	fprintf(_out, "player: %i\n", _player ? 1 : 0);
}

void Game::undo() 
{
	if(can_undo()) {
		_enemy[0]->undo(_board);
		_enemy[1]->undo(_board);
		set(_move_history[--_current_move - 1]);
		_player = !_player;
		if(!can_undo()) {
			printf("!!!!!!!!!!!!!!!\n");
			_board->initial_position();
		}
	}
}

void Game::redo()
{
	if(can_redo()) {
		set(_move_history[_current_move++]);
		_player = !_player;
	}
}

void Game::set(MoveHistoryEntry entry) {
	delete _board;
	_board = entry.board()->copy();
}

void Game::get_last_move(int& fromX, int &fromY, int& toX, int& toY)
{
	MoveHistoryEntry e = _move_history[_current_move - 1];
	fromX = e.from_x();
	fromY = e.from_y();
	toX = e.to_x();
	toY = e.to_y();
}

void Game::load(FILE *in)
{
	int back, eof, player, fromX, fromY, toX, toY;
	while(!feof(in)) {
		if(0 != (eof = fscanf(in, "%*i: %i %i %i %i", &fromX, &fromY, &toX, &toY))) {
			if(eof == EOF) break;
			printf("move: %i %i %i %i\n", fromX, fromY, toX, toY);
			move(fromX, fromY, toX, toY);
		} else if(0 != (eof = fscanf(in, "time: %*[^\n]"))) {
			if(eof == EOF) break;
			printf("time\n");
		} else if(0 != (eof = fscanf(in, "back: %i", &back))) {
			if(eof == EOF) break;
			printf("back: %i\n", back);
			for(int i = 0; i < back; i++)
				undo();
		} else if(0 != (eof = fscanf(in, "player: %i", &player))) {
			if(eof == EOF) break;
			printf("player: %i\n", player);
			set_player(player);
		}
	}
}

void Game::set_minimax_depth(int minimaxDepth)
{
	_enemy[0]->set_minimax_depth(minimaxDepth);
	_enemy[1]->set_minimax_depth(minimaxDepth);
}

bool Game::is_end()
{
	bool res = _board->is_end();
	//if(res) ;//fflush(_out);
	return res;
}

void Game::set_players(Enemy *a, Enemy *b)
{
	_enemy[0] = a;
	_enemy[1] = b;
}

void Game::reset()
{
	//while(can_undo())
		//undo();
	_board->initial_position();
	_player = 0;
}

void Game::print() {
	printf("\n\n");
	rep(i, 8) {
		rep(j, 8) {
			if(_board->get(0, i, j)) printf("x");
			else if(_board->get(1, i, j)) printf("o");
			else printf("_");
		}
		printf("\n");
	}
	printf("\n");
}

