/* Trabalho final da disciplina de Intelig�ncia Artificial
   Autores: Kau� Silveira - 171671 & Bruno Fiss - 171359
*/

#include "Game.h"
#include "Controller.h"
#include "Window.h"

#include <cstdio>
#include <stdlib.h>
#include <cassert>

#define DO_GENETICS false
 
int main(int argc, char **argv)
{
	setbuf(stdout, NULL);
  Game *game = new Game();
  Controller *controller = new Controller(game, DO_GENETICS);
  Window *window = new Window(argc, argv, controller);
  controller->set_window(window);
	if(argc > 1) {
		FILE *in = fopen(argv[1], "r");
		assert(in);
		printf("loading from file %s...\n", argv[1]);
		controller->load(in);
		printf("done!\n");
	}
	if(0) {
		//controller->play();
		while(game->think()) {
			if(game->is_end()) {
				printf("endgame\n");
				break;
			}
		}
	} else {
		window->main_loop();
	}
  return 0;
}

