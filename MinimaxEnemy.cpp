#include "MinimaxEnemy.h"
#include "Board.h"

#include <cstdio>
#include <climits>
//#include <iostream>
//using namespace std;
typedef MinimaxEnemy::HeuType HeuType;

HeuType maxhNull = -1
				, maxhCentro = -1
				, maxhConcentration = -1
				, maxhCentralisation = -1
				, maxhMasscenter = -1
				, maxhQuads = -1
				, maxhConnectedness = -1
				, maxhUniformity = -1
				;
typedef struct sHeu {
	int i;
	HeuType h;
} Heu;

static Board::MoveListOt g_MoveList[10];
//static Board::Set _think_boards[2] = {Board::Set(board_compare), Board::Set(board_compare)};
//#include <map>
//static std::map<Board::Comp, int> _board_value[2];
static Heu g_Heu[10][8 * 12];
static Board g_Boards[10][8 * 12];

int lessHeu(const void *l, const void *r) {
	return ((Heu *)l)->h - ((Heu *)r)->h;
}

int moreHeu(const void *l, const void *r) {
	return ((Heu *)r)->h - ((Heu *)l)->h;
}

void MinimaxEnemy::move(Board *board, int& fromX, int& fromY, int& toX, int& toY)
{
	//_game_boards.insert(board->to_comp());
	//_think_boards[0].clear();
	//_think_boards[1].clear();
	//_board_value[0].clear();
	//_board_value[1].clear();

	/*
	for(Board::Set::iterator i = _game_boards.begin(); i != _game_boards.end(); ++i) {
		cout << i->first << ' ' << i->second << endl;
	}
	cout << endl;//*/

  Board::MoveListOt moveList;
	board->all_moves(_player, moveList);

	alphabeta(*board, _minimax_depth, -HeuTypeMax, HeuTypeMax, _player);
	//_game_boards.insert(board->move_result(_player, moveList[_choice]).to_comp());

	//printf("_choice: %i\n", _choice); fflush(stdout);
	/*
  reps(i, moveList) {
		printf("%i: (%i, %i) -> (%i, %i)\n", i, INDEX_X(moveList[i].first), INDEX_Y(moveList[i].first)
				                           , INDEX_X(moveList[i].second), INDEX_Y(moveList[i].second));
		fflush(stdout);
  } //*/

	fromX = INDEX_X(INDEX_FROM(moveList[g_Heu[_minimax_depth][_choice].i]));
	fromY = INDEX_Y(INDEX_FROM(moveList[g_Heu[_minimax_depth][_choice].i]));
	toX = INDEX_X(INDEX_TO(moveList[g_Heu[_minimax_depth][_choice].i]));
	toY = INDEX_Y(INDEX_TO(moveList[g_Heu[_minimax_depth][_choice].i]));

	*board = g_Boards[_minimax_depth][g_Heu[_minimax_depth][_choice].i];
	//Board b;
	//b.initial_position();
	//*board = b;

#define PRINTHEU(x) \
	if(1) { \
		printf(#x ": %.5f %.5f\n", board->x[0], board->x[1]); \
	} 
	//PRINTHEU(centro);
	//PRINTHEU(concentration);
	//PRINTHEU(centralisation);
	//PRINTHEU(masscenter);
	//PRINTHEU(quads);
	//PRINTHEU(connectedness);
	//PRINTHEU(uniformity);
}

#define SQU(x) ((x) * (x))
#define ABS(x) ((x) > 0 ? (x) : (-(x)))
#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define MAX_ALPHABETA HeuTypeMax / 10000


// negamax com poda alphabeta
HeuType MinimaxEnemy::alphabeta(const Board &board, int depth, HeuType alpha, HeuType beta, int player)
{
	//printf("alphabeta(%i, %i, %i, %i)\n", depth, alpha, beta, player);

	if(board.is_end()) {
		//printf("board is end %i %i\n", board.win(0), board.win(1));
		int mul = 2 * player - 1; //1 -> 1; 0 -> -1
		mul *= 2 * _player - 1;
		if(board.win(_player)) return mul * MAX_ALPHABETA * (1 - 2 * board.win(!_player)) * (depth + 1); /* considerando vitoria simultanea */
		return -mul * MAX_ALPHABETA * (depth + 1); /* dando prioridade para o caminho mais curto */
	}
	if(depth == 0) {
		int mul = 2 * player - 1; //1 -> 1; 0 -> -1
		mul *= 2 * _player - 1;
		Board b = board;
		return mul * heuristica(b, _player, player);
	}

	/*
	if(_game_boards.count(board.to_comp())) { // empate
		return 0; // nao funciona, acaba sendo sempre a melhor escolha para ambos
	}*/

	//Board::MoveOt *moveList = g_MoveList[depth];
	int size = board.all_moves(player, g_MoveList[depth]);

/* sort */
	rep(i, size) {
		g_Heu[depth][i].h = heuristica(g_Boards[depth][i] = board.move_result(player, g_MoveList[depth][i]), _player, player);
		g_Heu[depth][i].i = i;
	}

	if(player != _player) {
		qsort(&g_Heu[depth], size, sizeof(Heu), lessHeu);
	} else {
		qsort(&g_Heu[depth], size, sizeof(Heu), moreHeu);
	}
	//Heu *perm = g_Heu[depth];
	//Board *boards = g_Boards[depth];

	/*
	if(depth == _minimax_depth) {
		rep(i, size)
			printf("%i(%i) ", g_Heu[depth][i].i, g_Heu[depth][i].h);
		printf("\n");
	}//*/
/* end sort */

	rep(i, size) {
		/*
		if(i / (float) size > 0.75) { // ignora os piores 
			break;
		}//*/
		//Board b = board.move_result(player, moveList[perm[i].i]);
		//if(_game_boards.count(b.to_comp())) { /* empate */
			//continue; // ignora posições que já foram atingidas antes
		//}
		//HeuType rec;
		//if(_think_boards[!player].count(b.to_comp())) { /* transposição */
		//	rec = -_board_value[!player][b.to_comp()];
		//} else {
		HeuType rec = -alphabeta(g_Boards[depth][g_Heu[depth][i].i], depth - 1, -beta, -alpha, !player);
		//}
		if(alpha < rec) {
			alpha = rec;
			if(depth == _minimax_depth) {
				//_choice = g_Heu[depth][i].i;
				_choice = i;
			}
		}
		if(alpha >= beta) {
			//_board_value[player][board.to_comp()] = alpha; _think_boards[player].insert(board.to_comp());
			return alpha;
		}
	}
	//_board_value[player][board.to_comp()] = alpha; _think_boards[player].insert(board.to_comp());
	return alpha;
}

#define FINDMAX(x) \
	if(x > MAX(1, max##x)) { \
		max##x = x; \
		printf(#x ": %f\n", x); \
	} 

HeuType MinimaxEnemy::heuristica(Board &board, bool player, bool plastmove)
{
	HeuType hNull = 0 //rand() / (double) RAND_MAX
					, hCentro = centro(board, player, plastmove)
					, hConcentration = 0 //concentration(board, player, plastmove)
					, hCentralisation = centralisation(board, player, plastmove)
					, hMasscenter = 0 //masscenter(board, player, plastmove)
					, hQuads = quads(board, player, plastmove)
					, hConnectedness = connectedness(board, player, plastmove)
					, hUniformity = 0 //uniformity(board, player, plastmove)
					;
	//FINDMAX(hCentro);
	//FINDMAX(hConcentration);
	//FINDMAX(hCentralisation);
	//FINDMAX(hMasscenter);
	//FINDMAX(hQuads);
	//FINDMAX(hConnectedness);
	//FINDMAX(hUniformity);
	return (hNull * _heu[0]
		+ hCentro * _heu[1]
		+ hConcentration * _heu[2]
		+ hCentralisation * _heu[3]
		+ hMasscenter * _heu[4]
		+ hQuads * _heu[5]
		+ hConnectedness * _heu[6]
		+ hUniformity * _heu[7]) / _heu_total
		//+ centro(board, player)
		//+ concentration(board, player)
		//+ centralisation(board, player)
		//+ masscenter(board, player)
		//+ quads(board, player)
		//+ connectedness(board, player)
		//+ uniformity(board, player)
		;
}
	
// centro: diferença entra as médias das distâncias para o centro
HeuType MinimaxEnemy::centro(Board &board, bool player, bool plastmove)
{
	HeuType res = 0;
	HeuType resb = 0;
	if(player == plastmove) {
		rep(i, 8)
			rep(j, 8)
				if(board.get(player, i, j))
					res += SQU(i - 3.5) + SQU(j - 3.5); // distância para o centro
		res /= (double) board.t[player].count();
		board.centro[player] = res;
		resb = board.centro[!player];
	} else {
		rep(i, 8)
			rep(j, 8)
				if(board.get(!player, i, j))
					resb += SQU(i - 3.5) + SQU(j - 3.5); // distância para o centro
		resb /= (double) board.t[!player].count();
		board.centro[!player] = resb;
		res = board.centro[player];
	}
	//return MAX_ALPHABETA - res;
	//printf("centro: %i %f %f\n", player, res, resb);
	return (resb - res) / 14.8;
}

HeuType MinimaxEnemy::concentration(Board &board, bool player, bool plastmove)
{
	static HeuType minsum[] = {0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 14, 16, 18, 20, 22};
	HeuType cmx = 0, cmy = 0;
	HeuType res = 0, resb = 0;
	int n = board.t[player].count();
	if(player == plastmove) {
		rep(i, 8)
			rep(j, 8)
				if(board.get(player, i, j)) {
					cmx += i;
					cmy += j;
				}
		cmx /= (double) n;
		cmy /= (double) n;
		board.cmx[player] = cmx;
		board.cmy[player] = cmy;
		//printf("cm: %f %f\n", cmx, cmy);

		HeuType sum = -minsum[n];
		rep(i, 8)
			rep(j, 8)
				if(board.get(player, i, j)) {
					HeuType dx = ABS(cmx - i);
					HeuType dy = ABS(cmy - j);
					sum += MAX(dx, dy);
				}
		res = sum;
		board.concentration[player] = res;
		resb = board.concentration[!player];
	} else {
		cmx = 0, cmy = 0;
		n = board.t[!player].count();
		rep(i, 8)
			rep(j, 8)
				if(board.get(!player, i, j)) {
					cmx += i;
					cmy += j;
				}
		cmx /= (double) n;
		cmy /= (double) n;
		board.cmx[!player] = cmx;
		board.cmy[!player] = cmy;

		HeuType sum = -minsum[n];
		rep(i, 8)
			rep(j, 8)
				if(board.get(!player, i, j)) {
					HeuType dx = ABS(cmx - i);
					HeuType dy = ABS(cmy - j);
					sum += MAX(dx, dy);
				}
		resb = sum;
		board.concentration[!player] = resb;
		res = board.concentration[player];
	}
	//printf(": %f %f\n", res, resb);
	return (resb - res) / 24;
}

HeuType MinimaxEnemy::centralisation(Board &board, bool player, bool plastmove)
{
	static
	int squval[8][8] = {{-80, -25, -20, -20, -20, -20, -25, -80},
			                {-25,  10,  10,  10,  10,  10,  10, -25},
											{-20,  10,  25,  25,  25,  25,  10, -20},
											{-20,  10,  25,  50,  50,  25,  10, -20},
											{-20,  10,  25,  50,  50,  25,  10, -20},
											{-20,  10,  25,  25,  25,  25,  10, -20},
			                {-25,  10,  10,  10,  10,  10,  10, -25},
											{-80, -25, -20, -20, -20, -20, -25, -80}};
	HeuType res = 0.0;
	HeuType resb = 0.0;

	if(player == plastmove) {
		rep(i, 8)
			rep(j, 8)
				if(board.get(player, i, j)) {
					res += squval[i][j];
				}
		res /= board.t[player].count();
		board.centralisation[player] = res;
		resb = board.centralisation[!player];
	} else {
		rep(i, 8)
			rep(j, 8)
				if(board.get(!player, i, j)) {
					resb += squval[i][j];
				}
		resb /= board.t[!player].count();
		board.centralisation[!player] = resb;
		res = board.centralisation[player];
	}
	//printf(": %f %f\n", res, resb);
	return (res - resb) / 63.1;
}

HeuType MinimaxEnemy::masscenter(Board &board, bool player, bool plastmove)
{
	HeuType cmx = 0, cmy = 0;
	HeuType res = 0.0, resb = 0.0;
	//int n = board.t[player].count();
	//rep(i, 8)
		//rep(j, 8)
			//if(board.get(player, i, j)) {
				//cmx += i;
				//cmy += j;
			//}
	//cmx /= (double) n;
	//cmy /= (double) n;
	if(player == plastmove) {
		cmx = board.cmx[player];
		cmy = board.cmy[player];
		res = SQU(cmx - 3.5) + SQU(cmy - 3.5);
		board.masscenter[player] = res;
		resb = board.masscenter[!player];
	} else {
		cmx = board.cmx[!player];
		cmy = board.cmy[!player];
		resb = SQU(cmx - 3.5) + SQU(cmy - 3.5);
		board.masscenter[!player] = resb;
		res = board.masscenter[player];
	}

	//cmx = 0, cmy = 0;
	//n = board.t[!player].count();
	//rep(i, 8)
		//rep(j, 8)
			//if(board.get(!player, i, j)) {
				//cmx += i;
				//cmy += j;
			//}
	//cmx /= (double) n;
	//cmy /= (double) n;
	//HeuType resb = SQU(cmx) + SQU(cmy);

	//printf(": %f %f\n", res, resb);
	return (res - resb) / 24.5;
}

HeuType MinimaxEnemy::quads(Board &board, bool player, bool plastmove)
{
	HeuType cmx = 0, cmy = 0;
	//int n = board.t[player].count();
	//rep(i, 8)
		//rep(j, 8)
			//if(board.get(player, i, j)) {
				//cmx += i;
				//cmy += j;
			//}
	//cmx /= (double) n;
	//cmy /= (double) n;
	HeuType res = 0;
	HeuType resb = 0;

	if(player == plastmove) {
		cmx = board.cmx[player];
		cmy = board.cmy[player];
		repbe(i, MAX(0, cmx - 2), MIN(6, cmx + 3)) {
			repbe(j, MAX(0, cmy - 2), MIN(6, cmy + 3)) {
				if(board.get(player, i, j) +
					 board.get(player, i + 1, j) + 
					 board.get(player, i, j + 1) + 
					 board.get(player, i + 1, j + 1) >= 3) {
					res++;
				}
			}
		}
		board.quads[player] = res;
		resb = board.quads[!player];
	} else {
		cmx = board.cmx[!player];
		cmy = board.cmy[!player];
		repbe(i, MAX(0, cmx - 2), MIN(6, cmx + 3)) {
			repbe(j, MAX(0, cmy - 2), MIN(6, cmy + 3)) {
				if(board.get(!player, i, j) +
					 board.get(!player, i + 1, j) + 
					 board.get(!player, i, j + 1) + 
					 board.get(!player, i + 1, j + 1) >= 3) {
					resb++;
				}
			}
		}
		board.quads[!player] = resb;
		res = board.quads[player];
	}
	//cmx = 0, cmy = 0;
	//n = board.t[!player].count();
	//rep(i, 8)
		//rep(j, 8)
			//if(board.get(!player, i, j)) {
				//cmx += i;
				//cmy += j;
			//}
	//cmx /= (double) n;
	//cmy /= (double) n;

	//printf(": %f %f\n", res, resb);
	return (res - resb) / 8.0;
}

HeuType MinimaxEnemy::connectedness(Board &board, bool player, bool plastmove)
{
	static int dx[] = {-1, -1, -1, 0, 0, 1, 1, 1},
						 dy[] = {-1, 0, 1, -1, 1, -1, 0, 1};

	HeuType res = 0;
	HeuType resb = 0;

	if(player == plastmove) {
		rep(i, 8)
			rep(j, 8)
				if(board.get(player, i, j)) {
					rep(d, 8) {
						int x = i + dx[d],
								y = j + dy[d];
						if(x < 0 || x > 7 ||
							 y < 0 || y > 7) {
							continue;
						}
						if(board.get(player, x, y)) {
							res++;
						}
					}
				}
		res /= (double) board.t[player].count();
		board.connectedness[player] = res;
		resb = board.connectedness[!player];
	} else {
		rep(i, 8)
			rep(j, 8)
				if(board.get(!player, i, j)) {
					rep(d, 8) {
						int x = i + dx[d],
								y = j + dy[d];
						if(x < 0 || x > 7 ||
							 y < 0 || y > 7) {
							continue;
						}
						if(board.get(!player, x, y)) {
							resb++;
						}
					}
				}
		resb /= (double) board.t[!player].count();
		board.connectedness[!player] = resb;
		res = board.connectedness[player];
	}
	//printf(": %f %f\n", res, resb);
	return (res - resb) / 4.0;
}

HeuType MinimaxEnemy::uniformity(Board &board, bool player, bool plastmove)
{
	HeuType res;
	HeuType resb;
	int minx, miny, maxx, maxy;

	if(player == plastmove) {
		minx = miny = 8;
		maxx = maxy = -1;
		rep(i, 8)
			rep(j, 8)
				if(board.get(player, i, j)) {
					if(i < minx) minx = i;
					if(i > maxx) maxx = i;
					if(j < miny) miny = j;
					if(j > maxy) maxy = j;
				}
		res = (maxx - minx + 1) * (maxy - miny + 1);
		board.uniformity[player] = res;
		resb = board.uniformity[!player];
	} else {
		minx = miny = 8;
		maxx = maxy = -1;
		rep(i, 8)
			rep(j, 8)
				if(board.get(!player, i, j)) {
					if(i < minx) minx = i;
					if(i > maxx) maxx = i;
					if(j < miny) miny = j;
					if(j > maxy) maxy = j;
				}
		resb = (maxx - minx + 1) * (maxy - miny + 1);
		board.uniformity[!player] = resb;
		res = board.uniformity[player];
	}
	//printf(": %f %f\n", res, resb);
	return (resb - res) / 52;
}

void MinimaxEnemy::set_weights(HeuType heu[NUM_HEU])
{
	_heu_total = 0;
	rep(i, NUM_HEU)
		_heu_total += _heu[i] = heu[i];
}

