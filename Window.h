#ifndef __IA_WINDOW_H__
#define __IA_WINDOW_H__
 

struct GLUI;
struct GLUI_RadioGroup;
struct GLUI_Spinner;
struct GLUI_Button;
class Controller;

class Window
{
public:
  Window(int argc, char** argv, Controller *control);
  ~Window();
  void main_loop();
  void enable_all();
  void disable_all();
	void enable(const char *s);
	void disable(const char *s);
	void display();
  
  /* GLUT callbacks */
  void render_scene(void);
  void special_keys(int key, int x, int y);
  void keyboard_func(unsigned char key, int x, int y);
  void reshape_func(int width, int height);
  void mouse_func(int button, int state, int x, int y);
  void timer_func(int key);
  void control_func(int id);

private:
  /* Desenho */
  void draw_tab();
  void draw_tab_bg();
  void draw_circle(int x, int y, int color);
  void draw_square(int x, int y, int color);

  int _window_id, _first_move, _computer_color, _minimax_depth, _width, _height;
  GLUI *glui;
  GLUI_RadioGroup *_RadioGroup_firstMove, *_RadioGroup_computerColor;
  GLUI_Spinner *_Spinner_minimax;
  GLUI_Button *_Button_start, *_Button_stop, *_Button_undo, *_Button_redo;
  Controller *_control;
};

#endif
