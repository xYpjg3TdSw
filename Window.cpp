#include "Window.h"
#include "Controller.h"
#include "common.h"

#include "GL/glui.h"
#include <cctype>
#include <cmath>
#include <cassert>

#define MAIN_WINDOW_WIDTH 500
#define MAIN_WINDOW_HEIGHT 500
#define MAIN_WINDOW_POSITION_X 100
#define MAIN_WINDOW_POSITION_Y 100
#define MAIN_WINDOW_TITLE "IA"

#define CHILD_WINDOW_POSITION_X 605
#define CHILD_WINDOW_POSITION_Y 100
#define CHILD_WINDOW_TITTLE "Op��es"
#define CHILD_WINDOW_PANEL_FIRST_MOVE_TITLE "Inicial:"

#define DEFAULT_MINIMAX_DEPTH 5
#define	DEFAULT_COMPUTER_COLOR 1

/* IDs para o callback da GLUI (ControlFunc) */
enum {
  ID_MINIMAX_DEPTH = 0,
  ID_FIRST_MOVE,
  ID_COMPUTER_COLOR,
  ID_PLAY_BUTTON,
  ID_STOP_BUTTON,
	ID_UNDO_BUTTON,
	ID_REDO_BUTTON
};

/* Para que os callbacks da GLUT possam acessar o objeto */
static Window *window = NULL;

/* GLUT callbacks wrappers */
void RenderScene(void) { window->render_scene(); }
void SpecialKeys(int key, int x, int y) { window->special_keys(key, x, y); }
void KeyboardFunc(unsigned char key, int x, int y) { window->keyboard_func(key, x, y); }
void ReshapeFunc(int width, int height) { window->reshape_func(width, height); }
void MouseFunc(int button, int state, int x, int y) { window->mouse_func(button, state, x, y); }
void TimerFunc(int key) { window->timer_func(key); }
void ControlFunc(int id) { window->control_func(id); }

Window::Window(int argc, char **argv, Controller *control)
: _control(control)
{
  window = this;
  
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  //glDisable(GL_DEPTH_TEST);
  glutInitWindowSize(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);
  glutInitWindowPosition(MAIN_WINDOW_POSITION_X, MAIN_WINDOW_POSITION_Y);
  
  _window_id = glutCreateWindow(MAIN_WINDOW_TITLE);
  
  glClearColor(223.0f / 255, 203.0f / 255, 155.0f / 255, 1.0f);
  
  // GLUT callbacks
  glutDisplayFunc(RenderScene);
  glutKeyboardFunc(KeyboardFunc);
  glutSpecialFunc(SpecialKeys);
  glutReshapeFunc(ReshapeFunc);
  glutMouseFunc(MouseFunc);
  //glutTimerFunc(TIME_INTERVAL, TimerFunc, 0);
  
  
  glui = GLUI_Master.create_glui(CHILD_WINDOW_TITTLE, 0, CHILD_WINDOW_POSITION_X, CHILD_WINDOW_POSITION_Y);
  
  GLUI_Panel *Panel_up = glui->add_panel("", GLUI_PANEL_NONE);
  
  GLUI_Panel *Panel_game = glui->add_panel_to_panel(Panel_up, "Jogo:");
  _Button_start = glui->add_button_to_panel(Panel_game, "Iniciar", ID_PLAY_BUTTON, ControlFunc);
  _Button_stop = glui->add_button_to_panel(Panel_game, "Parar", ID_STOP_BUTTON, ControlFunc);

	GLUI_Panel *Panel_undoRedo = glui->add_panel_to_panel(Panel_game, "", GLUI_PANEL_NONE);
	_Button_undo = glui->add_button_to_panel(Panel_undoRedo, "<-", ID_UNDO_BUTTON, ControlFunc);
	//glui->add_column_to_panel(Panel_undoRedo, false);
	_Button_redo = glui->add_button_to_panel(Panel_undoRedo, "->", ID_REDO_BUTTON, ControlFunc);
  
  glui->add_column_to_panel(Panel_up, false);
  
  GLUI_Panel *Panel_computerColor = glui->add_panel_to_panel(Panel_up, "Cor do Computador:");
  _RadioGroup_computerColor = glui->add_radiogroup_to_panel(Panel_computerColor, &_computer_color, ID_COMPUTER_COLOR,
                                                                            ControlFunc);
  glui->add_radiobutton_to_group(_RadioGroup_computerColor, "Nenhuma");
  glui->add_radiobutton_to_group(_RadioGroup_computerColor, "Brancas");
  glui->add_radiobutton_to_group(_RadioGroup_computerColor, "Pretas");
  glui->add_radiobutton_to_group(_RadioGroup_computerColor, "Ambas");
  _RadioGroup_computerColor->set_int_val(DEFAULT_COMPUTER_COLOR);
	_control->set_computer_color(DEFAULT_COMPUTER_COLOR);
  
  GLUI_Panel *Panel_down = glui->add_panel("", GLUI_PANEL_NONE);
  
  GLUI_Panel *Panel_firstMove = glui->add_panel_to_panel(Panel_down, CHILD_WINDOW_PANEL_FIRST_MOVE_TITLE);
  _RadioGroup_firstMove = glui->add_radiogroup_to_panel(Panel_firstMove, &_first_move, ID_FIRST_MOVE,
                                                                        ControlFunc);
  glui->add_radiobutton_to_group(_RadioGroup_firstMove, "Brancas");
  glui->add_radiobutton_to_group(_RadioGroup_firstMove, "Pretas");
  _RadioGroup_firstMove->set_int_val(0);
  
  glui->add_column_to_panel(Panel_down, false);
  
  GLUI_Panel *Panel_minimax = glui->add_panel_to_panel(Panel_down, "Minimax:");
  _Spinner_minimax = glui->add_spinner_to_panel(Panel_minimax, "Profundidade:", GLUI_SPINNER_INT,
                                                             &_minimax_depth, ID_MINIMAX_DEPTH, ControlFunc);
  _Spinner_minimax->set_int_limits(0, 10, GLUI_LIMIT_CLAMP);
  _Spinner_minimax->set_int_val(DEFAULT_MINIMAX_DEPTH);
  
  GLUI_Panel *Panel_about = glui->add_panel("Sobre:");
  glui->add_statictext_to_panel(Panel_about, "Disciplina: INF01048 - Inteligencia Artificial");
  glui->add_statictext_to_panel(Panel_about, "Professor: Paulo Martins Engel.");
  glui->add_statictext_to_panel(Panel_about, "Autores:");
  glui->add_statictext_to_panel(Panel_about, "  Bruno Coswig Fiss - 171359");
  glui->add_statictext_to_panel(Panel_about, "  Kaue Soares da Silveira - 171671");
  glui->add_statictext_to_panel(Panel_about, "Semestre: 2010/1");
  
  /** Tell GLUI window which other window to recognize as the main gfx window **/
  glui->set_main_gfx_window(_window_id);
  
  /** Register the Idle callback with GLUI (instead of with GLUT) **/
  //GLUI_Master.set_glutIdleFunc( myGlutIdle );

	// cool cursors: GLUT_CURSOR_INFO, GLUT_CURSOR_DESTROY, GLUT_CURSOR_WAIT, GLUT_CURSOR_CROSSHAIR, GLUT_CURSOR_NONE
  glutSetCursor(GLUT_CURSOR_INFO);
}

Window::~Window()
{  
  GLUI_Master.close_all();
}

void Window::main_loop()
{
	_control->stop();
	_control->set_minimax_depth(_minimax_depth);
  glutMainLoop();
}

void Window::enable_all()
{
  _RadioGroup_firstMove->enable();
  _RadioGroup_computerColor->enable();
  _Spinner_minimax->enable();
  _Button_start->enable();
  _Button_stop->enable();
  _Button_undo->enable();
  _Button_redo->enable();
}

void Window::disable_all()
{
  _RadioGroup_firstMove->disable();
  _RadioGroup_computerColor->disable();
  _Spinner_minimax->disable();
  _Button_start->disable();
  _Button_stop->disable();
  _Button_undo->disable();
  _Button_redo->disable();
}

void Window::enable(const char *s)
{
	if(!strcmp(s, "stop")) _Button_stop->enable();
	else if(!strcmp(s, "undo")) _Button_undo->enable();
	else if(!strcmp(s, "redo")) _Button_redo->enable();
	else assert("not found" && false);
}

void Window::disable(const char *s)
{
	if(!strcmp(s, "stop")) _Button_stop->disable();
	else if(!strcmp(s, "undo")) _Button_undo->disable();
	else if(!strcmp(s, "redo")) _Button_redo->disable();
	else assert("not found" && false);
}

void Window::display()
{
	glutPostRedisplay();
}

/* GLUT callacks */

void Window::render_scene(void)
{
  glClear(GL_COLOR_BUFFER_BIT);
  
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
	glViewport(0, 0, _width, _height);
	//GLUI_Master.auto_set_viewport();
  gluOrtho2D(0, _width, _height, 0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  
  glClearColor(223.0f / 255, 203.0f / 255, 155.0f / 255, 1.0f);
  
	draw_tab();
	
  glutSwapBuffers();

	_control->after_display();
}

void Window::special_keys(int key, int x, int y)
{
  switch(key) {
    case GLUT_KEY_UP:
      //KeyboardFunc('w', 0, 0);
      break;
    case GLUT_KEY_DOWN:
      //KeyboardFunc('s', 0, 0);
      break;
    case GLUT_KEY_RIGHT:
      //KeyboardFunc('d', 0, 0);
      break;
    case GLUT_KEY_LEFT:
      //KeyboardFunc('a', 0, 0);
      break;
    case GLUT_KEY_PAGE_UP:
      break;
  }
	glutPostRedisplay();
}

void Window::keyboard_func(unsigned char key, int x, int y)
{
  switch(tolower(key)) {
    case 'a':
      break;
    case 's':
			_control->stop();
      break;
    case 'd':
      break;
    case 'w':
      break;
    case 'h':
      break;
    case 'c':
      break;
    case 'r':
      break;
    case 'q':
      exit(0);
      break;
    default:
      break;
  }
  
  glutPostRedisplay(); 
}

void Window::reshape_func(int width, int height)
{
     _width = width;
     _height = height;
}
         
void Window::mouse_func(int button, int state, int x, int y)
{
  int s = MIN(_width, _height) / 8,
      rx = x / s,
      ry = y / s;
  
  switch(button) {
    case GLUT_LEFT_BUTTON:
      switch(state) {
        case GLUT_UP:
          _control->click(rx, ry);
          break;
      }
      break;
    case GLUT_RIGHT_BUTTON:
      switch(state) {
        case GLUT_UP:
          break;
      }
      break;
  }
  
	glutPostRedisplay();
}

void Window::timer_func(int key)
{
  //glutTimerFunc(TIME_INTERVAL, TimerFunc, 0);
  glutPostRedisplay();
}

/* GLUI callback */
void Window::control_func(int id)
{
  switch(id) {
    case ID_MINIMAX_DEPTH:
      _control->set_minimax_depth(_minimax_depth);
      break;
    case ID_FIRST_MOVE:
      _control->set_first_move(_first_move);
      break;
    case ID_COMPUTER_COLOR:
      _control->set_computer_color(_computer_color);
      break;
    case ID_PLAY_BUTTON:
      _control->play();
      break;
    case ID_STOP_BUTTON:
      _control->stop();
      break;
    case ID_UNDO_BUTTON:
      _control->undo();
      break;
    case ID_REDO_BUTTON:
      _control->redo();
      break;
  }
	glutPostRedisplay();
}

/* Desenho */

void Window::draw_tab()
{
  draw_tab_bg();
  
  rep(i, 8)
    rep(j, 8)
      if(!_control->is_empty(i, j))
        draw_circle(i, j, _control->piece(i, j));
}

void Window::draw_tab_bg()
{
	rep(i, 8)
    rep(j, 8) {
      if(_control->is_highlighted(i, j))
        draw_square(i, j, _control->highlight(i, j) + 2);
      else draw_square(i, j, (i + j) % 2);
    }
}

void Window::draw_circle(int x, int y, int c)
{
  double s = MIN(_width, _height) / 8,
      rx = x * s,
      ry = y * s;
  
  s /= 2;
  rx += s;
  ry += s;
  s *= 0.9;
  
  static
  float color[][3] = {{244.0f / 255, 244.0f / 255, 244.0f / 255},
                      {30.0f / 255, 30.0f / 255, 30.0f / 255}};
  
  glColor3f(color[c][0], color[c][1], color[c][2]);
  
  glBegin(GL_TRIANGLE_FAN);
  glVertex2f(rx, ry);
  repi(i, 380, 25) {
    double angle = i * 3.14 / 180;
    glVertex2f(rx + sin(angle) * s, ry + cos(angle) * s);
  }
  glEnd();
}

void Window::draw_square(int x, int y, int c)
{
  int s = MIN(_width, _height) / 8,
      rx = x * s,
      ry = y * s;
  
  static
  float color[][3] = {{230.0f / 255, 187.0f / 255, 130.0f / 255},
                      {218.0f / 255, 155.0f / 255,  71.0f / 255},
                      {0.0f, 0.0f, 1.0f},
                      {1.0f, 0.0f, 0.0f}
                     };
  
  glColor3f(color[c][0], color[c][1], color[c][2]);
  
  glBegin(GL_QUADS);
    glVertex2f(rx    , ry    );
    glVertex2f(rx    , ry + s);
    glVertex2f(rx + s, ry + s);
    glVertex2f(rx + s, ry    );
  glEnd();	
  
  
  glColor3f(0.0f, 0.0f, 0.0f);
  
  glBegin(GL_LINE_LOOP);
    glVertex2f(rx    , ry    );
    glVertex2f(rx    , ry + s);
    glVertex2f(rx + s, ry + s);
    glVertex2f(rx + s, ry    );
  glEnd();	
}
