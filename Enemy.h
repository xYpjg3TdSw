#ifndef __ENEMY_H__
#define __ENEMY_H__

class Board;

class Enemy
{
public:
	Enemy(bool player, int minimaxDepth): _player(player), _minimax_depth(minimaxDepth) {}
	virtual ~Enemy() {}
	virtual void move(Board *board, int& fromX, int& fromY, int& toX, int& toY) = 0;
	void set_minimax_depth(int minimaxDepth) { _minimax_depth = minimaxDepth; }
	virtual void undo(Board *board) {}
	inline void set_player(bool player) { _player = player; }

protected:
	bool _player;
	int _minimax_depth;
	Board *_board;

};

#endif
