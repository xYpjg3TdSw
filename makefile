#-g for debug
#-pg for profiling
#-O3 for optimization
CPPFLAGS = -Wall -O3 
LFLAGS = -lglui -lglut -lGLU -lGL -pg
OBJS = Board.o Controller.o Game.o main.o Window.o MoveHistoryEntry.o Enemy.o RandomEnemy.o MinimaxEnemy.o

all: ia
	ctags *
	./$<

ia: $(OBJS)
	g++ -o $@ $^ $(LFLAGS)

clean:
	rm -f *.o *~ *.out

prof:
	gprof -c ia > profiling

