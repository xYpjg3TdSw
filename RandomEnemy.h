#ifndef __RANDOM_ENEMY_H__
#define __RANDOM_ENEMY_H__

#include "Enemy.h"

#include <ctime>
#include <cstdlib>

class RandomEnemy: public Enemy
{
public:
	RandomEnemy(bool player, int minimaxDepth): Enemy(player, minimaxDepth) { srand((unsigned) time(NULL)); }
	void move(Board *board, int& fromX, int& fromY, int& toX, int& toY);
};

#endif
